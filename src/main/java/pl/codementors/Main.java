package pl.codementors;

import pl.codementors.Models.Bar;
import pl.codementors.Models.Barman;
import pl.codementors.Models.Client;
import pl.codementors.Models.Drink;

public class Main {

    public static void main(String[] args) {

        Barman barman = new Barman("Wóda");
        Client client1 = new Client("Edi");
        Client client2 = new Client("Edi");
        Client client3 = new Client("Edi");
        Client client4 = new Client("Edi");
        Client client5 = new Client("Edi");

        new Thread(barman).start();
        new Thread(client1).start();
//        new Thread(client2).start();
//        new Thread(client3).start();
//        new Thread(client4).start();
//        new Thread(client5).start();
    }
}
