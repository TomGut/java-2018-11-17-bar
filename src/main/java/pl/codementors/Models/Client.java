package pl.codementors.Models;

import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Client implements Runnable {

    private static final Logger log = Logger.getLogger(Client.class.getCanonicalName());

    private String name;
    private boolean running = true;

    public Client(){

    }

    public Client(String name){
        this.name = name;
    }


    @Override
    public void run() {
        Bar tmpBar = new Bar();
        while (running){
            try {
               tmpBar.getDrink().stream().map(Drink::getName).forEach(new Consumer<String>() {
                    @Override
                    public void accept(String s) {
                        System.out.println(s);
                    }
                });
                Thread.sleep(1000);
                tmpBar.takeDrink();
            } catch (InterruptedException ex) {
                log.log(Level.WARNING, ex.getMessage(), ex);
                //break;
            }
        }
    }
}