package pl.codementors.Models;

import java.util.ArrayList;
import java.util.List;

public class Bar {

    private static List<Drink> drinks = new ArrayList<>();
    private String drinkName;

    public Bar(){

    }

    public List<Drink> getDrink() {
        return drinks;
    }

    public synchronized void putDrink(String drinkName) {
        Drink drink = new Drink(drinkName);
        drinks.add(drink);
        System.out.println(drinks.size());
        notifyAll();
    }

    public synchronized List<Drink> takeDrink() throws InterruptedException {
        while (drinks.isEmpty()) {
            this.wait();
        }
        drinks.get(0);
        drinks.remove(0);
        return drinks;
    }

}
