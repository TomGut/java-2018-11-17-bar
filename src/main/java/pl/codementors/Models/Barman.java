package pl.codementors.Models;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Barman implements Runnable {

    private static final Logger log = Logger.getLogger(Barman.class.getCanonicalName());

    private String drinkName;
    private boolean running = true;

    public Barman(String drinkName){
        this.drinkName = drinkName;
    }

    @Override
    public void run() {
        Bar tmpBar = new Bar();
        while (running){
            try {
                Thread.sleep(500);
                tmpBar.putDrink(drinkName);
            } catch (InterruptedException ex) {
                log.log(Level.WARNING, ex.getMessage(), ex);
                //break;
            }
        }
    }
}
